FROM node:9

# RUN apk --no-cache add --virtual builds-deps build-base python

# use changes to package.json to force Docker not to use the cache
# when we change our application's nodejs dependencies:
ADD package*.json /tmp/
RUN cd /tmp && npm install --only=prod
#RUN npm rebuild --update-binary
RUN mkdir -p /opt/app && cp -a /tmp/node_modules /opt/app/

# From here we load our application's code in, therefore the previous docker
# "layer" thats been cached will be used if possible
WORKDIR /opt/app
ADD . /opt/app

EXPOSE 3000
CMD ["npm", "start"]
