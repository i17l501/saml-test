'use strict';

const Fs = require('fs');
const saml2 = require('samlify');
const request = require('request');
const express = require('express');

const app = express();

const entityID = process.env.SP_HOST;
const PORT = 3000;
const NAME_ID_FORMAT = 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified';
const REDIRECT_BINDING = 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect';
const POST_BINDING = 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST';

const loginTemplate = Fs.readFileSync('./loginTemplate.xml', {encoding: 'utf8'});

request('https://recette-sts.b1envenue.com/federationmetadata/2007-06/federationmetadata.xml', (err, response, body) => {
	const sp = saml2.ServiceProvider({
		assertionConsumerService: [{
			Binding: REDIRECT_BINDING,
			Location: `${entityID}/login_callback`
		},{
			Binding: POST_BINDING,
			Location: `${entityID}/login_callback`
		}],
		entityID,
		nameIDFormat: ['urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified'],
		loginNameIDFormat: 'unspecified',
		allowCreate: true,
		wantAssertionsSigned: false,
		loginRequestTemplate: {context: loginTemplate}
	});

	//Fs.writeFileSync('sp_metadata.xml', sp.getMetadata());
	const idp = saml2.IdentityProvider({
		metadata: body
	});

	app.get('/login', (req, res) => {
		const login = sp.createLoginRequest(idp, 'redirect', (template) => {
			const id = sp.entitySetting.generateID();
			const tags = {
				ID: id,
				Destination: idp.entityMeta.getSingleSignOnService('post'),
				Issuer: sp.entityMeta.getEntityID(),
				IssueInstant: new Date().toISOString(),
				NameIDFormat: NAME_ID_FORMAT,
				AssertionConsumerServiceURL: sp.entityMeta.getAssertionConsumerService('redirect'),
				EntityID: sp.entityMeta.getEntityID(),
				AllowCreate: sp.entitySetting.allowCreate,
			};
			Object.keys(tags).forEach(t => {
				template.context = template.context.replace(new RegExp(`{${t}}`, 'g'), tags[t]);
			});
			template.id = id;
			return template;
		});
		res.redirect(login.context);
	});

	app.post('/login_callback', express.urlencoded(), (req, res) => {
		sp.parseLoginResponse(idp, 'post', req)
			.then(parsedResult => {
				const attr = Object.keys(parsedResult.extract.attribute).reduce((memo, a) => {
					memo[a.split('/').pop()] = parsedResult.extract.attribute[a];
					return memo;
				},{});
				res.send(attr);
			})
			.catch(e => {
				console.error('Could not parse response',e);
				res.send(500);
			});
	});

	app.get('/metadata.xml', (req, res) => {
		res.type('application/xml');
		res.send(sp.getMetadata());
	});

	app.listen(PORT, () => console.log(`Started on ${PORT}`));
});
