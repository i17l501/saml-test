# Change log
Tous les changements importants de ce projet seront documentés ici.

Ce format est basé sur [Keep a Changelog](http://keepachangelog.com/) et ce projet respecte le [Semantic Versioning](http://semver.org/).

## [Unreleased]

[Unreleased]: BITBUCKET_URL/HEAD